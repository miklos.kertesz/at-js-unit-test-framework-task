import { expect } from 'chai';
import { describe, beforeEach, afterEach, it } from 'mocha';
import NumbersValidator from '../../app/numbers-validator.js';

describe('isInteger', () => {
    let validator;

    // initialize
    beforeEach(() => {
        validator = new NumbersValidator();
    });

    // cleanup
    afterEach(() => {
        validator = null;
    });

    // test case 1
    // returns true if the input value is a positive integer
    it('returns true if the input value is a positive integer', () => {
        expect(validator.isInteger(9)).to.be.equal(true);
    });

    // test case 2
    // returns true if the input value is a negative integer
    it('returns true if the input value is a negative integer', () => {
        expect(validator.isInteger(-9)).to.be.equal(true);
    });

    // test case 3
    // returns true if the input value is zero
    it('returns true if the input value is zero', () => {
        expect(validator.isInteger(0)).to.be.equal(true);
    });

    // test case 4
    // returns false if the input value a positive float
    it('returns false if the input value a positive float', () => {
        expect(validator.isInteger(9.99)).to.be.equal(false);
    });

    // test case 5
    // returns false if the input value a negative float
    it('returns false if the input value a negative float', () => {
        expect(validator.isInteger(-9.87)).to.be.equal(false);
    });

    // test case 6
    // throws an error if input is not a number
    it('throws an error if input is not a number', () => {
        expect(() => {
            validator.isInteger('sometest');
        }).to.throw('[sometest] is not a number');
    });
});