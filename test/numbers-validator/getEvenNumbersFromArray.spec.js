import { expect } from 'chai';
import { describe, beforeEach, afterEach, it } from 'mocha';
import NumbersValidator from '../../app/numbers-validator.js';

describe('getEvenNumbersFromArray', () => {
    let validator;
    // initialize
    beforeEach(() => {
        validator = new NumbersValidator();
    });

    //cleanup
    afterEach(() => {
        validator = null;
    });

    // test case 1
    // return array of even numbers
    it('should return an array of even numbers from an array of numbers', () => {
        expect(validator.getEvenNumbersFromArray([1, 2, 3, 4, 5, 6])).to.be.deep.equal([2, 4, 6]);
    });

    // test case 2
    // return empty array if input array has no even numbers
    it('should return an empty array if input array contains no even numbers', () => {
        expect(validator.getEvenNumbersFromArray([1, 3, 5, 7, 9, 11])).to.be.deep.equal([]);
    });

    // test case 3
    // throw error if input array contains non-number values
    it('should throw an error if the array contains non-number values', () => {
        expect(() => {
            validator.getEvenNumbersFromArray([1, 2, 3, 4, 5, '6', 7, 8, '9', '10']);
        }).to.throw('[1,2,3,4,5,6,7,8,9,10] is not an array of "Numbers"');
    });
});