import { expect } from 'chai';
import { describe, beforeEach, afterEach, it } from 'mocha';
import NumbersValidator from '../../app/numbers-validator.js';

describe('isAllNumbers', () => {
    let validator;

    // initialize
    beforeEach(() => {
        validator = new NumbersValidator();
    })

    // cleanup
    afterEach(() => {
        validator = null;
    });

    // test case 1
    // returns true if every element in an array is a number
    it('returns true if every element in an array is a number', () => {
        const testArray = [1, 2, 3, 4, 5, 6, 99, 10000000];
        expect(validator.isAllNumbers(testArray)).to.be.equal(true);
    });

    // test case 2
    // returns true if array is empty
    it('returns true for an empty array', () => {
        const emptyArray = [];
        expect(validator.isAllNumbers(emptyArray)).to.be.equal(true);
    });

    // test case 3
    // returns false if input array contains any item that is not a number
    it('returns false if not every element in an array is a number', () => {
        const testArray = [1, 2, 3, 4, 5, 6, 99, 10000000, '123456'];
        expect(validator.isAllNumbers(testArray)).to.be.equal(false);
    });

    // test case 4
    // throws an error if input is not an array
    it('throws an error if input is not an array', () => {
        const testValue = 12345;
        expect(() => {
            validator.isAllNumbers(testValue);
        }).to.throw(`[${testValue}] is not an array`);
    });
});